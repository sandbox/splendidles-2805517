<?php
/**
 * @file commerce_product_popup.admin.inc
 */

/**
 * Implements hook_form().
 * @return mixed
 */
function commerce_product_popup_admin_form() {
  $form = array();

  $form['commerce_product_popup_type'] = array(
    '#type'    => 'select',
    '#title'   => t('Dialog Type'),
    '#options' => array(
      'yes-no-action'=>t('Yes/No'),
    ),
    "#empty_option"=>t('- Select -'),
    '#ajax'   => array(
      'callback' => 'commerce_product_popup_admin_form_ajax',
      'method'   => 'replace',
      'wrapper'  => 'commerce-product-popup-config-block',
      'effect'   =>  'fade',
    ),
  );

  $form['commerce_product_popup_enabled'] = array(
    '#type'   => 'checkbox',
    '#title'  => t('Enable Popup?'),
  );

  $form['commerce_product_popup_config_block'] = array(
    '#type'   => 'markup',
    '#title'  => t('Popup Options'),
    '#prefix' => '<div id="commerce-product-popup-config-block">',
    '#suffix' => '</div>',
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function commerce_product_popup_form_commerce_product_popup_admin_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'commerce_product_popup_admin_form') {
    $path = drupal_get_path('module', 'commerce_product_popup');
    drupal_add_css("$path/css/commerce_product_popup.css");

    $values_set = isset($form['commerce_product_popup_values_set']);
    // fill out the initial values from the config on the first load
    if (!$values_set) {
      $popup_config = variable_get('commerce_product_popup_config', array());
      foreach($popup_config as $key=>$val) {
        $form_state['values'][$key] = $val;
        if (isset($form[$key])) {
          $form[$key]['#default_value'] = $val;
        }
      }
    }

    $form['commerce_product_popup_config_block']['#type'] = 'fieldset';
    // Dialog Option Configuration
    switch ($form_state['values']['commerce_product_popup_type']) {
      // YES/NO Dialog
      case 'yes-no-action':
        $form['commerce_product_popup_config_block']['commerce_product_popup_yes_response_text'] = array(
          '#type' => 'textfield',
          '#title' => t('Yes Response Text'),
          '#description' => t('The text you want displayed in the YES dialog box'),
          '#group' => 'commerce_product_popup_config_block',
          '#required' => TRUE,
        );
        $form['commerce_product_popup_config_block']['commerce_product_popup_no_response_text'] = array(
          '#type' => 'textfield',
          '#title' => t('No Response Text'),
          '#description' => t('The text you want displayed in the NO dialog box'),
          '#group' => 'commerce_product_popup_config_block',
          '#required' => TRUE,
        );
        if (isset($popup_config['commerce_product_popup_image']) &&
          $image = file_load($popup_config['commerce_product_popup_image'])) {
            $image_url = file_create_url($image->uri);
            $form['commerce_product_popup_config_block']['commerce_product_popup_image_preview'] = array(
              '#type' => 'markup',
              '#prefix' => '<div id="commerce-product-popup-admin-preview"><label>' . t('Popup Image') . '</label>',
              '#markup' =>  '<img src="' . $image_url . '"</img>',
            );
            $form['commerce_product_popup_config_block']['commerce_product_popup_image_remove'] = array(
              '#type' => 'submit',
              '#value' => 'Remove',
              '#ajax' => array(
                'callback' => 'commerce_product_popup_admin_remove_image',
                'method'   => 'replace',
                'wrapper'  => 'commerce-product-popup-admin-preview',
                'effect'   => 'fade',
              ),
              '#suffix' => '</div>',
            );
        }
        else {
          $form['commerce_product_popup_config_block']['commerce_product_popup_image'] = array(
            '#type' => 'file',
            '#title' => t('Popup Image'),
            '#description' => t('(Optional) The product image which will be displayed on the popup.'),
          );
        }
        $form['commerce_product_popup_config_block']['commerce_product_popup_action'] = array(
          '#type'   => 'select',
          '#title'  => t('Popup Action'),
          '#options' => array(
            'add-product-to-cart' => t('Add Product To Cart'),
          ),
          '#ajax'   => array(
            'callback' => 'commerce_product_popup_admin_form_action_ajax',
            'method'   => 'replace',
            'wrapper'  => 'commerce-product-popup-action-config-block',
            'effect'   =>  'fade',
          ),
          '#required' => TRUE,
        );
        $form['commerce_product_popup_config_block']['commerce_product_popup_action_block'] = array(
          '#type'   => 'markup',
          '#prefix' => '<div id="commerce-product-popup-action-config-block">',
          '#suffix' => '</div>',
        );
        $form_state['#rebuilt'] = TRUE;
        break;

      default:
        $form['commerce_product_popup_config_block']['#type'] = 'markup';
        break;
    }

    // Pop Up Action
    $form['commerce_product_popup_config_block']['commerce_product_popup_action_block']['#type'] = 'fieldset';
    switch ($form_state['values']['commerce_product_popup_action']) {

      case 'add-product-to-cart':
        $definition = commerce_product_popup_action_definitions('add-product-to-cart');
        $form['commerce_product_popup_config_block']['commerce_product_popup_action_block'] += $definition['settings form'];
        break;

      default:
        $form['commerce_product_popup_config_block']['commerce_product_popup_action_block']['#type'] = 'markup';
        break;
    }


    // fill in the default form values on first load
    if (isset($popup_config)) {
      foreach($popup_config as $key=>$val) {
        $form_state['values'][$key] = $val;
        if (isset($form['commerce_product_popup_config_block'][$key])) {
          $form['commerce_product_popup_config_block'][$key]['#default_value'] = $val;
        }
        elseif (isset($form['commerce_product_popup_config_block']['commerce_product_popup_action_block'][$key])) {
          $form['commerce_product_popup_config_block']['commerce_product_popup_action_block'][$key]['#default_value'] = $val;
        }
      }
    }

    // set the form value confirming the forms default values have been loaded.
    if (!isset($form['commerce_product_popup_values_set'])) {
      $form['commerce_product_popup_values_set'] = array('#type' => 'markup');
    }

    $form['#attributes'] = array('enctype' => 'multipart/form-data');
    $form['#submit'][] = 'commerce_product_popup_admin_form_submit';
  }
}

/**
 * defines action types for use with the checkout pop up, each definition
 * should have an "action callback" pointing to an actual callback function,
 * an array of "callback arguments" listing the variables found in the form
 * setting for the definition which matches the variable names there, and
 * a "settings form" which has the form components for the admin form settings.
 *   an action name must be sent to this function to identify each definition.
 *
 * "action callback" => "callback_function_goes here",
 * "callback arguments" => array("form_variable_1"),
 * "settings form" => array (
 *    "#title" => t("Action Settings Title"),
 *    "form_variable_1" => array(...)
 * ),
 *
 * @param $action
 * @return array
 */
function commerce_product_popup_action_definitions($action) {
  $definition = array();

  switch ($action) {
    case 'add-product-to-cart':
      $definition = array(
        // the callback action
        'action callback'     => 'commerce_product_popup_action_add_product_to_cart',
        // the arguments to pass to the function, should be the name of the settings form variables below
        'callback arguments'  => array('add_product_to_cart_product_id', 'add_product_to_cart_product_quantity'),
        // will be appended to the pop up action block in the system settings form
        'settings form' => array(
          '#title' => t('Add To Cart Settings'),
          'add_product_to_cart_product_id' => array(
            '#type' => 'textfield',
            '#title' => t('Product SKU'),
            '#required' => TRUE,
          ),
          'add_product_to_cart_product_quantity' => array(
            '#type' => 'textfield',
            '#title' => t('Product Quantity'),
            '#required' => TRUE,
          ),
        ),
      );
      break;

    default:
      break;
  }

  return $definition;
}

/**
 * ajax functionality triggered when selected a dialog type in the system
 * settings form
 * @param $form
 * @param $form_state
 * @return mixed
 */
function commerce_product_popup_admin_form_ajax(&$form, &$form_state) {
  return $form['commerce_product_popup_config_block'];
}

/**
 * ajax functionality triggered when selecting an action in the system settings
 * form
 * @param $form
 * @param $form_state
 * @return mixed
 */
function commerce_product_popup_admin_form_action_ajax(&$form, &$form_state) {
  return $form['commerce_product_popup_config_block']['commerce_product_popup_action_block'];
}


function commerce_product_popup_admin_remove_image(&$form, &$form_state) {
  if (!empty($form_state['values']['commerce_product_popup_image'])) {
    if($image = file_load($form_state['values']['commerce_product_popup_image'])) {
      $image->status = 0;
      file_delete($image);
    }
  }
  $form_state['values']['commerce_product_popup_image'] = "";
  variable_set('commerce_product_popup_config', $form_state['values']);
  return "";
}

/**
 * form submit handler for the admin form, simply stores the form
 * state values in a variable
 * @param $form
 * @param $form_state
 */
function commerce_product_popup_admin_form_submit(&$form, &$form_state) {
  // make sure the pop up image gets saved
  $popup_config = variable_get('commerce_product_popup_config', FALSE);
  if (!$popup_config['commerce_product_popup_image']) {
    if($image = file_save_upload('commerce_product_popup_image')) {
      $image->status = FILE_STATUS_PERMANENT;
      $image->destination = "public://commerce_product_popup/" . $image->filename;
      $image = file_copy($image, $image->destination);
      $form_state['values']['commerce_product_popup_image'] = $image->fid;
    }
  }
  else {
    $form_state['values']['commerce_product_popup_image'] = $popup_config['commerce_product_popup_image'];
  }

  variable_set('commerce_product_popup_config', $form_state['values']);
}
