(function($){

  Drupal.behaviors.commerce_product_popup = {

    "attach": function (context, settings) {

      if ($('#edit-commerce-product-popup-yes').length) {
        var yes_button = $('#edit-commerce-product-popup-yes');
        var no_button = $('#edit-commerce-product-popup-no');

        // on clicking checkout, display the popup
        if($('#edit-checkout').length && $('.commerce-product-popup-overlay').length) {
          $('#edit-checkout').click(function(event) {
            if ($('.commerce-product-popup-overlay').hasClass('hidden')) {
              event.preventDefault();
              $('.commerce-product-popup-overlay').removeClass('hidden');
            }
          });
        }

        // on clicking no, close the pop up
        no_button.click(function(event) {
          event.preventDefault();
          $('#edit-checkout').length ? $('#edit-checkout').click() : window.location.href = '/checkout';
        });

        // on clicking yes, run the action callback
        yes_button.click(function(event) {
          event.preventDefault();
          $.get('/checkout_popup_confirm', function() {
            $('#edit-checkout').length ? $('#edit-checkout').click() : window.location.href = '/checkout';
          });
        });


      }

    }

  };

})(jQuery);
